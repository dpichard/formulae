# Homebrew Formulae of my projects [![BSD3](https://img.shields.io/badge/LICENSE-BSD--3-red?logo=&style=for-the-badge)](./LICENSE)

---

## How to use it
Enter the following command in your terminal:

```bash
$ brew tap dpichard/formulae https://dpichard@bitbucket.org/dpichard/formulae.git
```

Then, install a software:
```bash
$ brew install dpichard/formulae/<PROJECT_NAME>
```

**Remark**: *Homebrew will install dependencies and build the project.*
