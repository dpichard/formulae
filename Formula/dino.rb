class Dino < Formula
  head do
    url "https://github.com/dino/dino.git"
  end

  depends_on "adwaita-icon-theme" => :build
  depends_on "glib" => :build
  depends_on "glib-networking" => :build
  depends_on "gpgme" => :build
  depends_on "icu4c" => :build
  depends_on "libgpg-error" => :build
  depends_on "libgcrypt" => :build
  depends_on "gtk+3" => :build
  depends_on "libsignal-protocol-c" => :build
  depends_on "libgee" => :build
  depends_on "libsoup" => :build
  depends_on "sqlite" => :build
  depends_on "cmake" => :build
  depends_on "gettext" => :build
  depends_on "ninja" => :build
  depends_on "vala" => :build
  depends_on "qrencode" => :build
  depends_on "libxml2" => :build


  def install
    # ENV.deparallelize  # if your formula fails when building in parallel
    # Remove unrecognized options if warned by configure
    system "./configure", "--disable-debug",
                          "--disable-dependency-tracking",
                          "--disable-silent-rules",
                          "--prefix=#{prefix}"
    # system "cmake", ".", *std_cmake_args
    system "make"

    # Fix filename of plugins
    system "mv", "build/plugins/http-files.dylib build/plugins/http-files.so" 
    system "mv", "build/plugins/omemo.dylib build/plugins/omemo.so" 
    system "mv", "build/plugins/openpgp.dylib build/plugins/openpgp.so" 
    
    system "make", "install"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test dino`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
