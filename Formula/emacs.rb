# coding: utf-8
class Emacs < Formula
  desc     "Official GNU/Emacs Text Editor ready out-of-the-box."
  homepage "https://www.gnu.org/software/emacs/"


  bottle do
    sha256 "9ab33f4386ca5f7326a8c28da1324556ec990f682a7ca88641203da0b42dbdae" => :catalina
  end

  stable do
    url    "https://ftp.gnu.org/gnu/emacs/emacs-27.1.tar.gz"
    mirror "https://ftpmirror.gnu.org/emacs/emacs-27.1.tar.gz"
    sha256 "ffbfa61dc951b92cf31ebe3efc86c5a9d4411a1222b8a4ae6716cfd0e2a584db"

    depends_on "autoconf"      => :build
    depends_on "automake"      => :build
    depends_on "gnu-sed"       => :build
    depends_on "pkg-config"    => :build
    depends_on "texinfo"       => :build

    depends_on "harfbuzz"      => :recommanded
    depends_on "jansson"       => :recommended
end

  head do
    url "https://github.com/emacs-mirror/emacs.git"

    depends_on "autoconf"      => :build
    depends_on "automake"      => :build
    depends_on "gnu-sed"       => :build
    depends_on "pkg-config"    => :build
    depends_on "texinfo"       => :build

    depends_on "harfbuzz"      => :recommended
    depends_on "jansson"       => :recommended
end


  # Dependencies:
  depends_on "gnutls"
  depends_on "dbus"          => :build
  depends_on "librsvg"       => :build
  depends_on "mailutils"     => :build
  depends_on "imagemagick@7" => :recommended



  # Options:
  option "with-xwidgets", "⚠ Experimental: build GNU/Emacs with xwigdet support"
  option "with-debug",    "⚠ build GNU/Emacs with debug features"



  def install
    args = %W[
      --disable-dependency-tracking
      --disable-silent-rules
      --enable-locallisppath=#{HOMEBREW_PREFIX}/share/emacs/site-lisp
      --infodir=#{info}/emacs
      --prefix=#{prefix}
      --without-x
      --with-xml2
      --with-dbus
      --with-json
      --with-gnutls
      --with-modules
      --with-harfbuzz
      --with-ns
      --disable-ns-self-contained
      --with-rsvg
      --without-pop
    ]


    # Note that if ./configure is passed --with-imagemagick but can't find the
    # library it does not fail but imagemagick support will not be available.
    # See: https://debbugs.gnu.org/cgi/bugreport.cgi?bug=24455
    args << "--with-imagemagick"
    
    imagemagick_lib_path = Formula["imagemagick"].opt_lib/"pkgconfig"
    ohai "ImageMagick PKG_CONFIG_PATH: ", imagemagick_lib_path
    ENV.prepend_path "PKG_CONFIG_PATH", imagemagick_lib_path


    if build.with? "debug"
      ohai "Enabling GNU/Emacs debug features..."
      args << "--disable-silent-rules"
      ENV.append_to_cflags "-g3"
    end


    if build.with? "xwidgets"
      args << "--with-xwidgets"
    end



    # Start the installation
    ENV.prepend_path "PATH", Formula["gnu-sed"].opt_libexec/"gnubin"
    system "./autogen.sh"
    system "./configure", *args
    

    system "make"
    system "make", "install"


    # We always build GNU/Emacs with cocoa
    prefix.install "nextstep/Emacs.app"


    # Replace the symlink with one that avoids starting Cocoa.
    (bin/"emacs").unlink # Kill the existing symlink
    (bin/"emacs").write <<-EOS
      #!/bin/bash
      exec #{prefix}/Emacs.app/Contents/MacOS/Emacs "$@"
    EOS

    # Follow MacPorts and don't install ctags from Emacs. This allows Vim
    # and Emacs and ctags to play together without violence.
    (bin/"ctags").unlink
    (man1/"ctags.1.gz").unlink
  end



  def caveats
    <<-EOS
      GNU/Emacs was installed to:
          #{prefix}

      To link GNU/Emacs to default Homebrew applications location:
          ln -s #{prefix}/Emacs.app/ /Applications/Emacs

      Please try the Cask for a better-supported Cocoa version:
          brew cask install emacs
    EOS
  end


  plist_options :manual => "emacs"


  def plist; <<-EOS
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
      <key>Label</key>
      <string>#{plist_name}</string>
      <key>ProgramArguments</key>
      <array>
        <string>#{opt_bin}/emacs</string>
        <string>--daemon</string>
      </array>
      <key>RunAtLoad</key>
      <true/>
    </dict>
    </plist>
    EOS
  end


  test do
    assert_equal "4", shell_output("#{bin}/emacs --batch --eval=\"(print (+ 2 2))\"").strip
  end
end
