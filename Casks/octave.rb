cask 'octave' do
  name     'Octave'
  version  '5.2.0'
  homepage 'https://www.gnu.org/software/octave/'

  url     'https://github.com/octave-app/octave-app/releases/download/v5.2.0-beta2/Octave-5.2.0-beta2.dmg'
  appcast 'https://github.com/octave-app/octave-app/releases'
  sha256  'cd821fe3a91ee42eb75891abd23f41139ae648b5904259e6835d84bbca32e2f0'

  conflicts_with formula: 'octave'

  app 'Octave-5.2.0.app'
end

