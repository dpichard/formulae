cask 'kdevelop' do
  name     'Octave'
  version  '5.3.2'
  homepage 'https://www.kdevelop.org/'

  url     'https://binary-factory.kde.org/job/KDevelop_Release_macos/480/artifact/kdevelop-5.3.2-480-macos-64-clang.dmg'
  appcast 'https://binary-factory.kde.org/job/KDevelop_Release_macos/'
  sha256  'ef29b528cbbb780328c23673ac05f5119ad8e276225c1199eef3a7ea75641566'

  conflicts_with formula: 'kdevelop'

  app 'KDevelop.app'
end

